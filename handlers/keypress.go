package handlers

import (
	"typing-app/services"
	"github.com/rivo/tview"
	"github.com/gdamore/tcell/v2"
	"time"
	"fmt"
)

const sampleText = "The quick brown fox jumps over the lazy dog."

func HandleInput(app *tview.Application, flex *tview.Flex, greeter, prompt, results *tview.TextView, inputField *tview.InputField) {
	var startTime time.Time

	greeter.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		startTime = time.Now()
		flex.Clear() // Clear items from the flex layout
		flex.AddItem(prompt, 2, 0, false).
			AddItem(inputField, 2, 0, true)
		prompt.SetText(sampleText)
		app.SetFocus(inputField)
		return event
	})

	inputField.SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEnter {
			elapsed := time.Since(startTime).Minutes()

			wpm := services.CalculateWPM(inputField.GetText(), elapsed)
			errorRate := services.CalculateErrorRate(inputField.GetText(), sampleText)

			resultsText := fmt.Sprintf("Test completed!\n\nWPM: %.2f\nErrors: %.2f%%", wpm, errorRate)
			flex.Clear() // Clear items again for the results view
			flex.AddItem(results, 0, 1, true)
			results.SetText(resultsText)
		}
	})
}

