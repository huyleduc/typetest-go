
package components

import "github.com/rivo/tview"

func NewGreeter() *tview.TextView {
	tv := tview.NewTextView().
		SetText("Welcome to the typing test!\nPress any key to begin...").
		SetTextAlign(tview.AlignCenter).
		SetDynamicColors(true).
		SetWrap(true)

	return tv
}

