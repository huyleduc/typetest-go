package components

import "github.com/rivo/tview"

func NewPrompt() *tview.TextView {
	tv := tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetWrap(true).
		SetDynamicColors(true)

	return tv
}

