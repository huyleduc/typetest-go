package components

import "github.com/rivo/tview"

func NewResults() *tview.TextView {
	return tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetText("")
}

