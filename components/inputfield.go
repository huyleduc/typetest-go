package components
import (
    "github.com/gdamore/tcell/v2"
)

import "github.com/rivo/tview"
func NewInputField() *tview.InputField {
	inputf := tview.NewInputField().
		SetFieldBackgroundColor(tcell.ColorBlack).
		SetFieldTextColor(tcell.ColorWhite)

	return inputf
}

