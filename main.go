package main

import (
	"typing-app/components"
	"typing-app/handlers"
	"github.com/rivo/tview"
)

func main() {
	app := tview.NewApplication()

	greeter := components.NewGreeter()
	prompt := components.NewPrompt()
	inputField := components.NewInputField()
	results := components.NewResults()

	flex := tview.NewFlex().SetDirection(tview.FlexRow).
		AddItem(nil, 1, 0, false).
		AddItem(greeter, 0, 1, true).
		AddItem(nil, 1, 0, false)

	handlers.HandleInput(app, flex, greeter, prompt, results, inputField)

	if err := app.SetRoot(flex, true).SetFocus(greeter).Run(); err != nil {
		panic(err)
	}
}


